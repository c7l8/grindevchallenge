//
//  JSONSerialization.swift
//  GrinDevChallengeTests
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

struct JSONSerialization {
    enum JSONSerializationError: Error {
        case invalidData
        case mockJSONFileNotFound
    }
    
    static func parseLocalJSON<T>(jsonFileName: String, in bundle: Bundle) throws -> T where T : Decodable  {
        if let filePath = bundle.path(forResource: jsonFileName, ofType: "json") {
            do {
                guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe) else {
                    throw JSONSerializationError.invalidData
                }
                
                let jsonDecoder = JSONDecoder()
                let object = try jsonDecoder.decode(T.self, from: data)
                
                return object
            } catch {
                throw error
            }
        } else {
            throw JSONSerializationError.mockJSONFileNotFound
        }
    }
    
    static func dataFromLocalJSON(jsonFileName: String, in bundle: Bundle) throws -> Data {
        if let filePath = bundle.path(forResource: jsonFileName, ofType: "json") {
            do {
                guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe) else {
                    throw JSONSerializationError.invalidData
                }
                
                return data
            } catch {
                throw error
            }
        } else {
            throw JSONSerializationError.mockJSONFileNotFound
        }
    }
}
