//
//  MockURLSession.swift
//  GrinDevChallengeTests
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

class MockURLSession: URLSession {
    var completionHandler:((Data?, URLResponse?, NSError?) -> Void)?
    
    var mockResponse: (data: Data?, urlResponse: URLResponse?, error: NSError?)
    
    var shared: URLSession {
        return MockURLSession()
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return MockSessionDataTask(response: self.mockResponse, completionHandler: completionHandler)
    }
    
    override func dataTask(with request: URLRequest) -> URLSessionDataTask {
        return MockSessionDataTask(response: self.mockResponse, completionHandler: completionHandler)
    }
}

class MockSessionDataTask: URLSessionDataTask {
    typealias Response = (data: Data?, urlResponse: URLResponse?, error: NSError?)
    
    var mockResponse: Response
    let completionHandler: ((Data?, URLResponse?, NSError?) -> Void)?
    
    init(response: Response, completionHandler:((Data?, URLResponse?, NSError?) -> Void)?) {
        self.mockResponse = response
        self.completionHandler = completionHandler
    }
    
    override func resume() {
        completionHandler!(mockResponse.data, mockResponse.urlResponse, mockResponse.error)
    }
}
