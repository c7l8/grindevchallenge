//
//  SaveDeviceTest.swift
//  GrinDevChallengeTests
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import XCTest
@testable import GrinDevChallenge

class SaveDeviceTest: XCTestCase {
    
    enum Constants {
        static let mockJSONFile = "MockSaveDeviceResponse"
        static let urlToMock = "http://mock.westcentralus.cloudapp.azure.com/grin_test/bluetooth/create"
    }    
    
    func testSaveDeviceRequest() {
        do {
            let bundle = Bundle(for: type(of: self))
            let mockJSONData = try JSONSerialization.dataFromLocalJSON(jsonFileName: Constants.mockJSONFile, in: bundle)
            XCTAssertNotNil(mockJSONData)
            
            let mockSession = MockURLSession()
            mockSession.mockResponse = (data: mockJSONData, urlResponse: nil, error: nil)
            let httpClient = GrinApiClient(session: mockSession)
            httpClient.saveDevice(name: "name", strength: "-23db") { result in
                switch result {
                case .success(_): break
                case .failure(let error):
                    XCTFail("Error: \(error)")
                }
            }
            
        } catch {
            XCTFail("Error: \(error)")
        }
    }
}
