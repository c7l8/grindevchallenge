//
//  BluetoothDevicesLocationListTest.swift
//  GrinDevChallengeTests
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import XCTest
@testable import GrinDevChallenge

class BluetoothDevicesLocationListTest: XCTestCase {
    
    enum Constants {
        static let mockJSONFile = "MockBluetoothDevicesLocationListJSON"
        static let urlToMock = "http://mock.westcentralus.cloudapp.azure.com/grin_test/bluetooth/all?order=1"
    }
    
    func testLoadDevicesRequest() {
        do {
            let bundle = Bundle(for: type(of: self))
            let mockJSONData = try JSONSerialization.dataFromLocalJSON(jsonFileName: Constants.mockJSONFile, in: bundle)
            XCTAssertNotNil(mockJSONData)
            
            let mockSession = MockURLSession()
            mockSession.mockResponse = (data: mockJSONData, urlResponse: nil, error: nil)
            let httpClient = GrinApiClient(session: mockSession)
            httpClient.loadDeviceMap { result in
                switch result {
                case .success(let devices):
                    XCTAssertNotNil(devices)
                    XCTAssert(devices.bluetoothDevices.count == 3)
                case .failure(let error):
                    XCTFail("Error: \(error)")
                }
            }
            
        } catch {
            XCTFail("Error: \(error)")
        }
        
    }
    
    func testBluetoothDevicesLocationListParse() {
        do {
            let bundle = Bundle(for: type(of: self))
            let storefrontParseResult: BluetoothDevicesLocationList = try JSONSerialization.parseLocalJSON(jsonFileName: Constants.mockJSONFile, in: bundle)
            XCTAssertNotNil(storefrontParseResult)
        } catch {
            XCTFail("Error: \(error)")
        }
    }

}
