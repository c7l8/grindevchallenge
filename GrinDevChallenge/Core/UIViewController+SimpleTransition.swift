//
//  UIViewController+SimpleTransition.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit
import os.log

extension UIViewController {
    func transition(to childViewController: UIViewController, completion: ((Bool) -> Void)? = nil) {
        let animationDuration = 0.3
        
        let current = children.last
        addChild(childViewController)
        
        if let childView = childViewController.view {
            childView.translatesAutoresizingMaskIntoConstraints = true
            childView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            childView.frame = view.bounds
            
            if let currentViewController = current {
                currentViewController.willMove(toParent: nil)
                
                transition(from: currentViewController,
                           to: childViewController,
                           duration: animationDuration,
                           options: [.transitionCrossDissolve],
                           animations: { },
                           completion: { done in
                            currentViewController.removeFromParent()
                            childViewController.didMove(toParent: self)
                            completion?(done)
                })
            } else {
                view.addSubview(childView)
                
                UIView.animate(withDuration: animationDuration,
                               delay: 0,
                               options: [.transitionCrossDissolve],
                               animations: {},
                               completion: { done in
                                childViewController.didMove(toParent: self)
                                completion?(done)
                })
            }
        } else {
            os_log("There is no childViewController!")
        }
    }
}
