//
//  Result.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(Error)
}
