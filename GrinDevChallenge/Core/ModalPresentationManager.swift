//
//  ModalPresentationManager.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit

class ModalPresentationManager: NSObject {
    var height: CGFloat?
}

extension ModalPresentationManager: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = ModalPresentationController(presentedViewController: presented, presenting: presenting, height: self.height)
        return presentationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ModalPresentationAnimator(isPresentation: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ModalPresentationAnimator(isPresentation: false)
    }
}
