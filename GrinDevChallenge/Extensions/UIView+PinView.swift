//
//  UIView+PinView.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/28/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit

public extension UIView {
    public func pin(to view: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: view.leadingAnchor),
            trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topAnchor.constraint(equalTo: view.topAnchor),
            bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }
}

