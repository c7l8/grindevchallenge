//
//  LoadingViewController.swift
//  GriniDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.

import UIKit

class LoadingViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
