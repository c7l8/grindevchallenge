//
//  MessageViewController.swift
//  GriniDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.

import UIKit

protocol MessageViewControllerDelegate: class {
    func messageViewControllerActionButtonWasTapped(_ messageVC: MessageViewController)
}

class MessageViewController: UIViewController {
    weak var delegate: MessageViewControllerDelegate?
    
    private let message: String
    private let buttonLabel: String
    
    @IBOutlet var messageLabel: UILabel?
    @IBOutlet var actionButton: UIButton?
    
    init(message: String, buttonLabel: String = "Retry") {
        self.message = message
        self.buttonLabel = buttonLabel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MessageViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel?.text = message
        actionButton?.setTitle(buttonLabel, for: .normal)
    }
}

extension MessageViewController {
    @IBAction func tryAgain(_ sender: UIButton) {
        delegate?.messageViewControllerActionButtonWasTapped(self)
    }
}
