//
//  BluetoothDevice.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

struct BluetoothDevice: Codable {
    let id, created, strength, name: String
    let location: Location
}
