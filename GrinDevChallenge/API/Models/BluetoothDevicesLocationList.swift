//
//  BluetoothDevicesLocationList.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

struct BluetoothDevicesLocationList: Codable {
    let status, datetime: String
    let bluetoothDevices: [BluetoothDevice]
    
    enum CodingKeys: String, CodingKey {
        case status, datetime
        case bluetoothDevices = "objects"
    }
}
