//
//  SaveDeviceStatus.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

struct SaveDeviceStatus: Codable {
    let status, datetime: String
    let object: Object
}

extension SaveDeviceStatus: CustomStringConvertible {
    var description: String {
        return """
        Save status: \(self.status)
        Date: \(self.datetime)
        id: \(self.object.id)
        name: \(self.object.name)
        strength: \(self.object.strength)
        """
    }
}
