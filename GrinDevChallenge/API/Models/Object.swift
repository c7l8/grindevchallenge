//
//  Object.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

struct Object: Codable {
    let id, name, strength: String
}
