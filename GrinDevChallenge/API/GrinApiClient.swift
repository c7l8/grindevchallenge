//
//  GrinApiClient.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

class GrinApiClient: SimpleHttpClient {
    
}

extension GrinApiClient: DeviceMapProvider {
    func loadDeviceMap(_ completion: @escaping (Result<BluetoothDevicesLocationList>) -> Void) {        
        struct DevicesEndPoint: EndPoint {
            var url = URL(string: "http://mock.westcentralus.cloudapp.azure.com/grin_test/bluetooth/all?order=1")!
            var httpMethod: HTTPMethod = .get
            var task: HttpTask = .request
        }
        
        self.request(for: DevicesEndPoint(), callback: completion)
    }
}

extension GrinApiClient: SaveBluetoothDeviceProvider {
    func saveDevice(name: String, strength: String, completion: @escaping (Result<SaveDeviceStatus>) -> Void) {
        struct SaveDevicesEndPoint: EndPoint {
            var url = URL(string: "http://mock.westcentralus.cloudapp.azure.com/grin_test/bluetooth/create")!
            var httpMethod: HTTPMethod = .post
            var task: HttpTask
            
            init(name: String, strength: String) {
                task = .requestWithParameters(bodyParams: ["name": name, "strength" : strength])
            }
        }
        
        self.request(for: SaveDevicesEndPoint(name: name, strength: strength), callback: completion)
    }
}
