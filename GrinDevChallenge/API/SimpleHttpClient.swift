//
//  HttpClient.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

typealias Parameters = [String:Any]

enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
}

enum HttpTask {
    case request
    case requestWithParameters(bodyParams: [String: Any])
}

protocol EndPoint {
    var url: URL { get }
    var httpMethod: HTTPMethod { get }
    var task: HttpTask { get }
}

enum NetworkError: Error {
    case encodingFailure
}

class SimpleHttpClient {
    private let session: URLSession
    
    enum HttpClientError: Error {
        case invalidData
    }
    
    init(session: URLSession = .shared) {
        self.session = session
    }
}

extension SimpleHttpClient {
    func request<T: Decodable>(for endPoint: EndPoint, callback: @escaping (Result<T>) -> Void) {
        do {
            let request = try buildRequest(for: endPoint)
            let task = session.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    return callback(.failure(error))
                }
                
                if let data = data {
                    do {
                        let decodedJSON = try JSONDecoder().decode(T.self, from: data)
                        return callback(.success(decodedJSON))
                        
                    } catch let error {
                        return callback(.failure(error))
                    }
                } else {
                    return callback(.failure(HttpClientError.invalidData))
                }
            }
            task.resume()
        } catch {
            return callback(.failure(error))
        }
    }
}

private extension SimpleHttpClient {
    func buildRequest(for endPoint: EndPoint) throws -> URLRequest {
        var request = URLRequest(url: endPoint.url)
        request.httpMethod = endPoint.httpMethod.rawValue
        
        do {
            switch endPoint.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-type")
            case .requestWithParameters(let bodyParams):
                let data = try JSONSerialization.data(withJSONObject: bodyParams, options: [])
                request.httpBody = data
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
            }
        } catch {
            throw NetworkError.encodingFailure
        }
        
        return request
    }
}
