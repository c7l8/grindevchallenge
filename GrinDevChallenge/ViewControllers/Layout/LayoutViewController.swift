//
//  LayoutViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit

class LayoutViewController: UIViewController {

    @IBOutlet weak var itemStackViewContainer1: UIStackView!
    @IBOutlet weak var itemStackViewContainer2: UIStackView!
    @IBOutlet weak var itemStackViewContainer3: UIStackView!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var optionStackView: UIStackView!
}

extension LayoutViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupItemsBackground()
        setupTextViewBorder()
        setupOptionStackShadow()
    }
}

private extension LayoutViewController {
    
    func setupItemsBackground() {
        let itemViews = [itemStackViewContainer1, itemStackViewContainer2, itemStackViewContainer3]
        
        for stackView in itemViews {
            let bgView = UIView()
            bgView.backgroundColor = UIColor.lightGray
            bgView.layer.cornerRadius = 10.0
            bgView.layer.borderColor = UIColor.darkGray.cgColor
            bgView.layer.borderWidth = 1.0
            
            if let stackView = stackView {
                self.pinBackground(bgView, to: stackView)
            }
        }
    }
    
    func setupTextViewBorder() {
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.darkGray.cgColor
        textView.layer.cornerRadius = 10.0
    }
    
    func setupOptionStackShadow() {
        let bgView = UIView()
        bgView.backgroundColor = UIColor.white
        
        bgView.layer.masksToBounds = false
        bgView.layer.shadowRadius = 5.0
        bgView.layer.shadowOffset =  CGSize(width: 0, height: -4)
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOpacity = 1
        
        pinBackground(bgView, to: optionStackView)
    }
    
    func pinBackground(_ view: UIView, to stackView: UIStackView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        stackView.insertSubview(view, at: 0)
        view.pin(to: stackView)
    }
}

