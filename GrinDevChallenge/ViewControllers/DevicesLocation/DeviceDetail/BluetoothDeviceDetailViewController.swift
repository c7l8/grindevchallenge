//
//  BluetoothDeviceDetailViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit

class BluetoothDeviceDetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    private var device: BluetoothDevice?
    
    static func make(device: BluetoothDevice) -> BluetoothDeviceDetailViewController {
        let storyboard = UIStoryboard(name: "BluetoothDeviceDetailViewController", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BluetoothDeviceDetailViewController") as! BluetoothDeviceDetailViewController
        
        vc.device = device
        
        return vc
    }
}

extension BluetoothDeviceDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = device?.name
        idLabel.text = device?.id
        strengthLabel.text = device?.strength
        dateLabel.text = device?.created
    }
}
