//
//  BTPinAnnotation.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import MapKit

class BTPinAnnotation: NSObject, MKAnnotation {
    private(set) var coordinate: CLLocationCoordinate2D
    private(set) var device: BluetoothDevice
    
    init(device: BluetoothDevice) {
        self.device = device
        self.coordinate = device.coordinate
    }
}

extension BTPinAnnotation {
    static func createViewAnnotationForMap(mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        var annotationView: BTAnnotationView?
        
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: BTAnnotationView.annotationIdentifier) as? BTAnnotationView {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = BTAnnotationView(annotation: annotation, reuseIdentifier: BTAnnotationView.annotationIdentifier)
        }
        
        annotationView?.state = .unselected
        
        return annotationView
    }
}
