//
//  BTAnnotationView.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import MapKit

class BTAnnotationView: MKAnnotationView {
    static let annotationIdentifier = "BTAnnotationIdentifier"
    
    enum PinAnnotationState {
        case selected
        case unselected
        
        var image: UIImage? {
            switch self {
            case .selected:
                return UIImage(named: "map-marker_on")
            case .unselected:
                return UIImage(named: "map-marker_off")
            }
        }
    }
    
    var state: PinAnnotationState = .unselected {
        didSet {
            self.image = state.image
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            state = .selected
        } else {
            state = .unselected
        }
    }
}
