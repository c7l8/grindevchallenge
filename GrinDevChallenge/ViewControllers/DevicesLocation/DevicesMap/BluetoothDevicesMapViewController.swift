//
//  DevicesMapViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit
import MapKit

protocol BluetoothDevicesAnnotationsControllerDelegate: class {
    func bluetoothDevicesAnnotationsController(_ bluetoothDevicesAnnotationsController: BluetoothDevicesAnnotationsController, didSelectDevice device: BluetoothDevice)
    func bluetoothDevicesAnnotationsControllerReload(_ bluetoothDevicesAnnotationsController: BluetoothDevicesAnnotationsController)
}

class BluetoothDevicesAnnotationsController: UIViewController {
    
    weak var delegate: BluetoothDevicesAnnotationsControllerDelegate?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var reloadButton: UIButton!
    
    private var deviceList: BluetoothDevicesLocationList?
    
    static func make(bluetoothDevices: BluetoothDevicesLocationList) -> BluetoothDevicesAnnotationsController {
        let storyboard = UIStoryboard(name: "BluetoothDevicesAnnotationsController", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BluetoothDevicesAnnotationsController") as! BluetoothDevicesAnnotationsController
        
        vc.deviceList = bluetoothDevices
        
        return vc
    }
}

extension BluetoothDevicesAnnotationsController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadButton.addTarget(self, action: #selector(self.reload(sender:)), for: .touchUpInside)
        
        self.mapView.delegate = self
        if let deviceList = deviceList {
            updateDevicesInMap(deviceList: deviceList)
        }
    }
}

private extension BluetoothDevicesAnnotationsController {
    func updateDevicesInMap(deviceList: BluetoothDevicesLocationList) {
        let arrayOfAnotations: [BTPinAnnotation] = deviceList.bluetoothDevices.map { BTPinAnnotation(device: $0)  }        
        self.mapView.showAnnotations(arrayOfAnotations, animated: true)
    }
    
    @objc func reload(sender: UIButton) {
        delegate?.bluetoothDevicesAnnotationsControllerReload(self)
    }
}

extension BluetoothDevicesAnnotationsController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return BTPinAnnotation.createViewAnnotationForMap(mapView:mapView, annotation: annotation)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let btPin = view.annotation as? BTPinAnnotation {
            delegate?.bluetoothDevicesAnnotationsController(self, didSelectDevice: btPin.device)
        }
    }
}
