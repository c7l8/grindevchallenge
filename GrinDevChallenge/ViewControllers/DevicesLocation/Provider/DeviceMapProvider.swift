//
//  DeviceMapProvider.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

protocol DeviceMapProvider {
    func loadDeviceMap(_ completion: @escaping (Result<BluetoothDevicesLocationList>) -> Void)
}
