//
//  DevicesLocationViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit

class DevicesLocationViewController: UIViewController {
    var loader: DeviceMapProvider?
    
    private lazy var presentationManager = ModalPresentationManager()
}

extension DevicesLocationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDeviceList()
    }
}

extension DevicesLocationViewController {
    func fetchDeviceList() {
        transition(to: LoadingViewController()) { _ in                        
            self.loader?.loadDeviceMap { result in
                switch result {
                case .success(let devices):
                    DispatchQueue.main.async {
                        let deviceListVC = BluetoothDevicesAnnotationsController.make(bluetoothDevices: devices)
                        deviceListVC.delegate = self
                        self.transition(to: deviceListVC)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        let errorVC = MessageViewController(message: error.localizedDescription)
                        errorVC.delegate = self
                        self.transition(to: errorVC)
                    }
                }
            }
        }
    }
}

extension DevicesLocationViewController: BluetoothDevicesAnnotationsControllerDelegate {
    func bluetoothDevicesAnnotationsController(_ bluetoothDevicesAnnotationsController: BluetoothDevicesAnnotationsController, didSelectDevice device: BluetoothDevice) {
        presentationManager.height = 400
        let detailVC = BluetoothDeviceDetailViewController.make(device: device)
//        detailVC.delegate = self
        detailVC.transitioningDelegate = presentationManager
        detailVC.modalPresentationStyle = .custom
        self.present(detailVC, animated: true)
    }
    
    func bluetoothDevicesAnnotationsControllerReload(_ bluetoothDevicesAnnotationsController: BluetoothDevicesAnnotationsController) {
        fetchDeviceList()
    }
}

extension DevicesLocationViewController: MessageViewControllerDelegate {
    func messageViewControllerActionButtonWasTapped(_ messageVC: MessageViewController) {
        self.fetchDeviceList()
    }
}
