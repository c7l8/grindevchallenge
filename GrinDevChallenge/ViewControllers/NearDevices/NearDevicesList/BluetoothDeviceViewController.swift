//
//  BluetoothDeviceViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit
import os.log
import CoreBluetooth

protocol BluetoothDeviceViewControllerDelegate: class {
    func bluetoothDeviceViewController(_ bluetoothDeviceViewController: BluetoothDeviceViewController, didSelectDevice device: CBPeripheral)
    func bluetoothDeviceViewControllerReload(_ bluetoothDeviceViewController: BluetoothDeviceViewController)
}

class BluetoothDeviceViewController: UIViewController {
    
    weak var delegate: BluetoothDeviceViewControllerDelegate?

    @IBOutlet private weak var tableView: UITableView!
    
    private var deviceList: [CBPeripheral]?
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    private enum Constants {
        static let cellIdentifier = "btDeviceCellIdentifier"
    }
    
    static func make(bluetoothDevices: [CBPeripheral]) -> BluetoothDeviceViewController {
        let storyboard = UIStoryboard(name: "BluetoothDeviceViewController", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BluetoothDeviceViewController") as! BluetoothDeviceViewController
        
        vc.deviceList = bluetoothDevices
        
        return vc
    }
}

extension BluetoothDeviceViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addSubview(self.refreshControl)
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension BluetoothDeviceViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let deviceList = deviceList else {
            return 0
        }
        
        return deviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: Constants.cellIdentifier)
        }
        
        if let deviceList = deviceList {
            let theDevice = deviceList[indexPath.row]
            cell?.textLabel?.text = theDevice.name ?? "Unknow"
            cell?.detailTextLabel?.text = theDevice.identifier.uuidString
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let deviceList = deviceList else {
            os_log("There is no deviceList!")
            return
        }
        
        let theDevice = deviceList[indexPath.row]
        delegate?.bluetoothDeviceViewController(self, didSelectDevice: theDevice)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        delegate?.bluetoothDeviceViewControllerReload(self)
    }
}
