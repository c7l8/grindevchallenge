//
//  NearBluetoothDevicesViewController.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/25/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import UIKit
import CoreBluetooth

class NearBluetoothDevicesViewController: UIViewController {    
    private var loader: NearBluetoothDevicesProvider = NearBluetoothDeviceLoader()
    var saveProvider: SaveBluetoothDeviceProvider?
}

extension NearBluetoothDevicesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let start = MessageViewController(message: "Look for near bluetooth devices", buttonLabel: "Go!")
        start.delegate = self
        transition(to: start)
    }
}

extension NearBluetoothDevicesViewController {
    func retrieveNearBluetoothDevices() {
        transition(to: LoadingViewController()) { _ in
            self.loader.loadNearBluetoothDevices { result in
                switch result {
                case .success(let devices):
                    let deviceListVC = BluetoothDeviceViewController.make(bluetoothDevices: devices)
                    deviceListVC.delegate = self
                    self.transition(to: deviceListVC)
                case .failure(let error):
                    let errorVC = MessageViewController(message: "\(error)")
                    errorVC.delegate = self
                    self.transition(to: errorVC)
                }
            }
        }
    }
}

extension NearBluetoothDevicesViewController: MessageViewControllerDelegate {
    func messageViewControllerActionButtonWasTapped(_ messageVC: MessageViewController) {
        self.retrieveNearBluetoothDevices()
    }
}

extension NearBluetoothDevicesViewController: BluetoothDeviceViewControllerDelegate {
    func bluetoothDeviceViewControllerReload(_ bluetoothDeviceViewController: BluetoothDeviceViewController) {
        self.retrieveNearBluetoothDevices()
    }
    
    func bluetoothDeviceViewController(_ bluetoothDeviceViewController: BluetoothDeviceViewController, didSelectDevice device: CBPeripheral) {
        let optionMenu = UIAlertController(title: nil, message: "Do you want to save the device?", preferredStyle: .actionSheet)
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            self.save(device: device)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func save(device: CBPeripheral) {
        let savingAlertView = UIAlertController(title: "Saving...", message: nil, preferredStyle: .alert)
        self.present(savingAlertView, animated: true, completion: nil)        
        self.saveProvider?.saveDevice(name: device.name ?? "Unknow", strength: device.identifier.uuidString, completion: { result in
            savingAlertView.dismiss(animated: true, completion: {
                switch result {
                case .success(let status):
                    self.presentAlert(withTitle: "Sucess", message: "\(status)")
                case .failure(let error):
                    self.presentAlert(withTitle: "Error", message: "\(error)")
                }
            })
        })
    }
}
