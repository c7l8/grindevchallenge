//
//  NearBluetoothDevicesProvider.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol NearBluetoothDevicesProvider {
    func loadNearBluetoothDevices(_ completion: @escaping (Result<[CBPeripheral]>) -> Void)
}
