//
//  NearBluetoothDeviceLoader.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/26/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation
import CoreBluetooth
import os.log

class NearBluetoothDeviceLoader: NSObject, NearBluetoothDevicesProvider {
    private var centralManager: CBCentralManager!
    private var peripherals = Set<CBPeripheral>()
    private var timer = Timer()
    private var completion: ((Result<[CBPeripheral]>) -> Void)?
    
    private enum NearBluetoothDeviceLoaderError: Error {
        case notAvailable
        case unauthorized
        case poweredOff
    }
    
    private enum Constants {
        static let scanTimeInterval: TimeInterval = 12
    }
    
    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
}

extension NearBluetoothDeviceLoader {
    func loadNearBluetoothDevices(_ completion: @escaping (Result<[CBPeripheral]>) -> Void) {
        self.completion = completion
        startScan()
    }
}


extension NearBluetoothDeviceLoader {
    func startScan() {
        os_log("Now Scanning...")
        
        self.peripherals = []
        guard self.centralManager.state == .poweredOn else {
            self.completion?(.failure(NearBluetoothDeviceLoaderError.notAvailable))
            return
        }
        
        self.timer.invalidate()
        centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        self.timer = Timer.scheduledTimer(timeInterval: Constants.scanTimeInterval, target: self, selector: #selector(self.stopScanAndSendResponse), userInfo: nil, repeats: false)
    }
    
    @objc func stopScan() {
        os_log("stopScan")
        self.centralManager.stopScan()
    }
    
    @objc private func stopScanAndSendResponse() {
        os_log("Scan Stopped. Peripherals Found: %@", "\(peripherals.count)")
        self.centralManager.stopScan()
        self.completion?(.success(Array(peripherals)))
    }
}

extension NearBluetoothDeviceLoader: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown, .resetting, .unsupported:
            self.completion?(.failure(NearBluetoothDeviceLoaderError.notAvailable))
            stopScan()
        case .unauthorized:
            self.completion?(.failure(NearBluetoothDeviceLoaderError.unauthorized))
            stopScan()
        case .poweredOff:
            self.completion?(.failure(NearBluetoothDeviceLoaderError.poweredOff))
            stopScan()
        case .poweredOn: break
        }
    }
}

extension NearBluetoothDeviceLoader: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        peripherals.insert(peripheral)
    }
}
