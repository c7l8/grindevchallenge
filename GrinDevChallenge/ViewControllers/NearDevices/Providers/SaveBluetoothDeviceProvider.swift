//
//  SaveBluetoothDeviceProvider.swift
//  GrinDevChallenge
//
//  Created by Daniel Nieto on 12/27/18.
//  Copyright © 2018 Daniel Nieto. All rights reserved.
//

import Foundation

protocol SaveBluetoothDeviceProvider {
    func saveDevice(name: String, strength: String, completion: @escaping (Result<SaveDeviceStatus>) -> Void)
}
